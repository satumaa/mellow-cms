A docker implemention to run coaster CMS:

*   Built with Laravel 5 (v5.5)
*   Responsive file manager
*   WYSIWYG editor
*   Block based templating system
*   Beacon support

## Quick Start

You will still need the following installed locally:

*   php 7
*   [composer](https://getcomposer.org/)
*   [docker for osx](https://store.docker.com/editions/community/docker-ce-desktop-mac)
    (standard dmg install only, no extra toolbox needs installing on recent os)

Ensure Docker is running (you should see the whale icon in the top bar) then start up the container from the project root directory by running:
docker-composer up

Application runs on
localhost:8080

## Key file and locations

Coaster settings, encluding database in .env

MySQL content stored in data/ folder

Container MySQL accessible through location "mysql" (not localhost)

Database settings must match those in docker-composer.yml

## How to modify docker

To make changes to the docker file, first make sure the docker is not running:
docker-composer down

Then make wanted changes to Dockerfile and or docker-compose.yml and run:
docker-composer build

Then restart by running:
docker-composer up

## Modifying the application

With exception to changes in the docker settings, all other changes can be made without needing to close down docker, and will take effect without any delay.

This includes using composer to add new packages etc.
