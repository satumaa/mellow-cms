<?php namespace CoasterCms\Http\Controllers\AdminControllers;

use Auth;
use CoasterCms\Facades\FormMessage;
use CoasterCms\Http\Controllers\AdminController as Controller;
use CoasterCms\Models\AdminLog;
use CoasterCms\Models\User;
use CoasterCms\Models\UserRole;
use CoasterCms\Models\Block;
use CoasterCms\Models\BlockRepeater;
use CoasterCms\Models\PageBlockRepeaterData;
use CoasterCms\Models\PageBlockRepeaterRows;
use CoasterCms\Models\PageBlock;

class VimeoController extends Controller
{

    public function AddNewBlock(){

            //create a block name
            $blockName=Block::create([
                'category_id'=>9,
                'label'=>'merki blog',
                'name'=>'merki',
                'type'=>'repeater'
            ]);

            //create block informations
            $blockVideoUrl=Block::create([
                'category_id'=>1,
                'label'=>'mirki title',
                'name'=>'mirki_title',
                'type'=>'string'
            ]);
            $blockVideoInfo=Block::create([
                'category_id'=>1,
                'label'=>'Mirki Content',
                'name'=>'mirki_content',
                'type'=>'richtext'
            ]);
            $blockVideoImage=Block::create([
                'category_id'=>1,
                'label'=>'Mirki image',
                'name'=>'mirki_image',
                'type'=>'image'
            ]);
       
            //video url and info in block repeaters
            $repeaterArry=array();

            $repeaterArry[0]=$blockVideoUrl->id;
            $repeaterArry[1]=$blockVideoInfo->id;
            $repeaterArry[2]=$blockVideoImage->id;

            $repeater=BlockRepeater::create([
                'block_id'=>$blockName->id,
                'blocks'=>$repeaterArry
            ]);

            //place two rows for two repater datas - video url and info
            $PageBlockRepeaterRows=PageBlockRepeaterRows::Create([
                'repeater_id'=>$repeater->id,
                'row_id'=>1,
            ]);

            $PageBlockRepeaterRows=PageBlockRepeaterRows::Create([
                'repeater_id'=>$repeater->id,
                'row_id'=>2
            ]);
            $PageBlockRepeaterRows=PageBlockRepeaterRows::Create([
                'repeater_id'=>$repeater->id,
                'row_id'=>3
            ]);

            //Actual data
            $blockData=PageBlockRepeaterData::create([
                'row_key'=>1,
                'block_id'=>$blockVideoUrl->id,
                'content'=>'Mikä merki ?',
                'version'=>1,
            ]);

            $blockData=PageBlockRepeaterData::create([
                'row_key'=>1,
                'block_id'=>$blockVideoInfo->id,
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s',
                'version'=>1,
            ]);
            $blockData=PageBlockRepeaterData::create([
                'row_key'=>1,
                'block_id'=>$blockVideoImage->id,
                'content'=>'https://www.publicdomainpictures.net/pictures/10000/velka/1-1254836694ahTH.jpg',
                'version'=>1,
            ]);
            


            //indicate the page to display
            PageBlock::create([
                'page_id'=>1,
                'block_id'=>$blockVideoUrl->id,
                'content'=>'Mikä merki ?',
                'version'=>1
                ]);

            PageBlock::create([
                'page_id'=>1,
                'block_id'=>$blockVideoInfo->id,
                'content'=>'Some sample content here',
                'version'=>1
                ]);
            PageBlock::create([
                'page_id'=>1,
                'block_id'=>$blockVideoImage->id,
                'content'=>'https://www.publicdomainpictures.net/pictures/10000/velka/1-1254836694ahTH.jpg',
                'version'=>1
                ]);
                //inform repeater active
            PageBlock::create([
                'page_id'=>1,
                'block_id'=>$blockName->id,
                'content'=>1,
                'version'=>1
                ]);


                return 'Added a new block';
    }

}
