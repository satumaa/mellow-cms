<?php namespace CoasterCms\Http\Controllers\AdminControllers;

use CoasterCms\Http\Controllers\AdminController as Controller;
use CoasterCms\Models\Block;
use Request;
use CoasterCms\Models\BlockRepeater;
use Response;
use View;
use CoasterCms\Models\PageBlockRepeaterData;
use Session;



class RepeatersController extends Controller
{
    /*
    public function getIndex(){
        $block = Block::where('name','vimeo')->first();
        
        $repeaterRow=BlockRepeater::where('block_id',$block->id)->first();

        $repaterArr = explode(',',$repeaterRow->blocks);
        // dd($repeaterRow);
        $repeaterFields=array();
        for($i=0; $i<count($repaterArr);$i++){
            $repeaterFields[$i]=Block::find((int)$repaterArr[$i]);
        }

        $this->layoutData['content'] = View::make('coaster::pages.pages.newRepeater', [
           'repeater_name'=>$block->name,
           'repeater_fields'=>$repeaterFields
        ]);
    }
    */

    public function postIndex(Request $request)
    {

       
        $block = Block::find(Request::input('block_id'));
        if  (($repeaterId = Request::input('repeater_id')) && $block && $block->type == 'repeater') {
            return $block->setPageId(Request::input('page_id'))->getTypeObject()->edit($repeaterId, true);
        }
        return 0;
       

         /*
        $idArray=Request::input('block_id_url');
        $data = array(
            array('row_key'=>2, 'block_id'=>$idArray[0],'content'=>Request::input('vimeo_url'),'version'=>1),
            array('row_key'=>2, 'block_id'=>$idArray[1],'content'=>Request::input('vimeo_info'),'version'=>1),

        );
        PageBlockRepeaterData::insert($data);
        Session::flash('success','The video block added');
        return redirect()->back();
         */

    }

}
