<?php namespace CoasterCms\Http\Controllers\AdminControllers;

use Auth;
use View;
use CoasterCms\Http\Controllers\AdminController as Controller;
use Request;
use File;
use Session;
use CoasterCms\Models\Template;
use CoasterCms\Models\Theme;
use CoasterCms\Models\ThemeTemplate;

class TemplateController extends Controller {

    public function createTemplate() {

        $this->layoutData['content'] = View::make('coaster::template.new-template');
    }

    public function storeTemplate() {
        
        $template=Template::create([
            'label'=>Request::input('templateName ').'Template',
            'template'=>Request::input('templateName')
        ]);
        

        $themeId=$this->getThemeId();
        
        ThemeTemplate::create([
            'theme_id'=>$themeId,
            'template_id'=>$template->id
        ]);
        
        $templatePath=base_path().'/vendor/web-feet/coasterframework/resources/views/admin/template/';
        $fileName=Request::input('templateName');
        File::put($templatePath.$fileName.'.blade.php','');
        
        Session::flash('success','A new template - '.$fileName.'.blade.php'. ' - created successfully in template folder');

        return redirect()->back();
        
    }

    public function getThemeId(){
        $thumbs = [];
        $themeID;

        $themesPath = base_path('resources/views/themes');
        if (is_dir($themesPath)) {
            foreach (Theme::all() as $databaseTheme) {
                $databaseThemes[$databaseTheme->theme] = $databaseTheme;
            }
            foreach (scandir($themesPath) as $themeFolder) {
                if (strpos($themeFolder, '.') !== 0) {

                    $theme = new \stdClass;
                    if (isset($databaseThemes[$themeFolder])) {
                        $theme->id = $databaseThemes[$themeFolder]->id;
                        $theme->install = 0;
                        $theme->active = ($databaseThemes[$themeFolder]->id == config('coaster::frontend.theme'));

                        if($theme->active ){
                            $themeID = $databaseThemes[$themeFolder]->id;
                        }
                       
                    } 
                }
            }
        }

        return $themeID;

    }
    

}