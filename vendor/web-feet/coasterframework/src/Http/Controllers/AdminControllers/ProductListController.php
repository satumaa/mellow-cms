<?php
use Auth;
use CoasterCms\Facades\FormMessage;
use CoasterCms\Http\Controllers\AdminController as Controller;
use CoasterCms\Models\AdminLog;
use CoasterCms\Models\User;
use CoasterCms\Models\UserRole;
use CoasterCms\Models\Block;
use CoasterCms\Models\BlockRepeater;
use CoasterCms\Models\PageBlockRepeaterData;
use CoasterCms\Models\PageBlockRepeaterRows;
use CoasterCms\Models\PageBlock;

class ProductListController extends Controller {

      public function AddProductsListBlock() {
        $blockName=Block::create([
            'category_id'=>5,
            'label'=>'Products',
            'name'=>'products',
            'type'=>'repeater'
        ]);
      }

}
