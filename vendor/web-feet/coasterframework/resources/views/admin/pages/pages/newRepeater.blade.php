
<h1>Add new {{$repeater_name}} </h1>
@if(Session::has('success'))
<div class="alert alert-success" role="alert">
    {{Session::get('success')}}
  </div>
@endif
{!! Form::open(['class' => 'form-horizontal', 'id' => 'addForm', 'enctype' => 'multipart/form-data']) !!}
@foreach($repeater_fields as $field)
<div class="form-group">
{!! Form::label('label', $field->label, ['class' => 'control-label col-sm-2']) !!}

<div class="col-sm-10">
    <div class="row">
        <div class="col-sm-9">{!! Form::text($field->name, '', ['class' => 'form-control','required']) !!}</div>
    </div>
</div>
<div class="col-sm-12">
    <div class="row">
        {!! Form::hidden('block_id_url[]', $field->id, ['class' => 'form-control']) !!}
    </div>
</div>

@endforeach



<div class="col-sm-10">
    <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> &nbsp;
        Add {{$repeater_name}}</button>
</div>
{!!Form::close()!!}
