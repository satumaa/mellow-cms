<h1>Let's set up an alias for your account</h1>

<br />

<p>If not empty, it can be displayed in certain themes rather than the account email.</p>

<br/>

{!! $form !!}
