<div class="container">
    <div class="row">
        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{Session::get('success')}}
        </div>
       
        @endif
        <h1>Create a new template</h1>
        <form method="POST" action="storetemplate">
               {{csrf_field()}}
            <div class="form-group">
                <label for="templateName">Name of the template</label>
                <input type="text" name="templateName" class="form-control" placeholder="new-template-name" required>
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>