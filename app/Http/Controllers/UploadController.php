<?
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;


class UploadController extends Controller
{

    public function upload(Request $request)
    {

        if(Request::hasFile('file')){

            $file = Request::file('file');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/uploads/';
            return $file->move($path, $filename);
        }

    }
}
?>
