

@if($is_first)
    <div class="row page-feature">
@endif

<div class="col-sm-4">
    <div class="feature-image">
        {!! PageBuilder::block('content_image', ['class' => 'img-responsive']) !!}
    </div>
    <h3 class="text-center">{{ PageBuilder::block('title') }}</h3>
    
    <p class="justify">{{ PageBuilder::block('content', ['length' => 150]) }}</p>
    <div class="text-center">
        <a href="{!! $page->url !!}" class="btn btn-default">More info</a>
    </div>
    

</div>

@if($is_last)
</div>
@endif