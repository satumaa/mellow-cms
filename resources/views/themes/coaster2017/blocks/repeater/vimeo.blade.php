@if ($is_first)
<div id="main-banner" class="carousel slide" data-ride="carousel">

    @if ($total > 1)
    <!-- Indicators -->
    <a href="#main-banner" role="button" data-slide="prev">
    <ol class="carousel-indicators">
        @for ($i = 0; $i < $total; $i++)
        <li data-target="main-banner" data-slide-to="{{ $i }}"{!! ($i==0)?' class="active"':'' !!}></li>
        @endfor
    </ol>
    @endif
</a>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
@endif


        @if(strpos(PageBuilder::block('vimeo_url'),'https://www.youtube.com/') !==false)

        <div class="item {{ ($count==1)?'active':'' }}">
            <iframe
            src={!!PageBuilder::block('vimeo_url')!!}?autoplay=1&loop=1&showinfo=0&controls=0&disablekb=1&rel=0&mute=1 frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
            </iframe>

            <div class="vimeo-info">
                {!!PageBuilder::block('vimeo_info')!!}<br>
                <a class="btn" role="button" href="products/range/starter-product">Youtube</a>
            </div>
            
         </div>

         @elseif(strpos(PageBuilder::block('vimeo_url'),'.mp4') !==false)
         <div class="item {{ ($count==1)?'active':'' }}">
            <video autoplay loop>
                    <source src="{!! PageBuilder::block('vimeo_url') !!}" type="video/mp4">
                    Your browser does not support HTML5 video.
            </video>
            <div class="vimeo-info">
                    {!!PageBuilder::block('vimeo_info')!!}<br>
                    <a class="btn" role="button" href="products/range/starter-product">Vimeo</a>
                </div>
        </div>
        @else

        <div class="item {{ ($count==1)?'active':'' }}" style="background:url('{{ PageBuilder::block('slide_image', ['view' => 'raw']) }}')">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="carwrap">
                          <{{ ($count == 1)?'h1':'h2' }}>{{ PageBuilder::block('vimeo_info') }}</{{ ($count == 1)?'h1':'h2' }}>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endif

@if ($is_last)
    </div>

    {{-- @if ($total > 1)
    <!-- Controls -->
    <a class="left carousel-control" href="#main-banner" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#main-banner" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    @endif --}}

</div>
@endif
