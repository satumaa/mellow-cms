<div class="row">
    <div class="col-sm-12">
        <h1>{!! PageBuilder::block('title') !!}</h1>
        <p class="lead">{!! PageBuilder::block('lead_text') !!}</p>
        {!! PageBuilder::block('content') !!}
    </div>
</div>

<div class="">
    {!! PageBuilder::block('team') !!} 
   
</div>