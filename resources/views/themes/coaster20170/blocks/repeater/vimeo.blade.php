@if ($is_first)
<div id="main-banner" class="carousel slide" data-ride="carousel">

    @if ($total > 1)
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @for ($i = 0; $i < $total; $i++)
        <li data-target="main-banner" data-slide-to="{{ $i }}"{!! ($i==0)?' class="active"':'' !!}></li>
        @endfor
    </ol>
    @endif

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
@endif
        
    
        @if(strpos(PageBuilder::block('vimeo_url'),'https://www.youtube.com/') !==false)
        
        <div class="item {{ ($count==1)?'active':'' }}">
            <iframe
            src={!!PageBuilder::block('vimeo_url')!!}?autoplay=1&loop=1&showinfo=0&controls=0&disablekb=1&rel=0&mute=1 frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
            </iframe>
         </div>
        
        @else

        <div class="item {{ ($count==1)?'active':'' }}" style="background:url('{{ PageBuilder::block('slide_image', ['view' => 'raw']) }}')">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="carwrap">
                           <!--more buttonb removed-->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endif

@if ($is_last)
    </div>

    @if ($total > 1)
    <!-- Controls -->
    <a class="left carousel-control" href="#main-banner" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#main-banner" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    @endif

</div>
@endif