{!! PageBuilder::section('head') !!}


{!! PageBuilder::block('vimeo') !!}


<section id="sec1">
    <div class="container">
        @if ($pageId = PageBuilder::block_selectpage('show_sub_pages'))
        {!! PageBuilder::category(['page_id' => $pageId, 'view' => 'home', 'limit' => 3]) !!}
        @endif
    </div>
</section>

<section class="about-us">
    <div class="container">
        <div class="row">

                <h3 class="text-center about-us-header">{!! PageBuilder::block('title')!!}</h3>
                <p class="about-us-text"></i>{!! PageBuilder::block('lead_text')!!}</p>
        </div>
       
    </div>
</section>
 
{!! PageBuilder::block('banner') !!}

{!! PageBuilder::section('footer') !!}
